import React from 'react'

var FormSubmit = React.createClass({
  render: function() {
    return (
      <div className="formSubmit">
        <button type="button" className="save btn btn-primary">Save</button>
        <button type="button" className="cancel btn btn-default">Cancel</button>
      </div>
    );
  }
});

export default FormSubmit
