import React from 'react'
import ItemList from './ItemList/index.jsx'

var ItemPicker = React.createClass({
  render: function() {
    return (
      <div className="itemList">
        <ItemList />
      </div>
    );
  }
});

export default ItemPicker
