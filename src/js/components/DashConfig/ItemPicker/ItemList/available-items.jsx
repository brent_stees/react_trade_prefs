import React from 'react'
import Item from './Item/index.jsx'

var AvailableItems = React.createClass({
  render: function() {
    return (
      <div className="availableItems">
        <Item />
      </div>
    );
  }
});

export default AvailableItems