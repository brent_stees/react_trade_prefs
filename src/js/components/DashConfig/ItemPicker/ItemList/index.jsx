import React from 'react'
// import AvailableItems from './available-items.jsx'
// import VisibleItems from './visible-items.jsx'
import VisibleItems from '../../../lib/Container.jsx'
import AvailableItems from '../../../lib/Container.jsx'

var VisibleProps = [
  {
    title: "Symbol & Description",
    required: "true",
    id: 1
  },
  {
    title: "Change %",
    required: "false",
    id: 2
  },
  {
    title: "Change",
    required: "false",
    id: 3
  },
  {
    title: "Last",
    required: "false",
    id: 4
  },
  {
    title: "Last Volume",
    required: "false",
    id: 5
  },
  {
    title: "Bid",
    required: "false",
    id: 6
  },
  {
    title: "Bid Size",
    required: "false",
    id: 7
  },
  {
    title: "Ask",
    required: "false",
    id: 8
  },
  {
    title: "Ask Size",
    required: "false",
    id: 9
  },
  {
    title: "Last",
    required: "false",
    id: 10
  },
  {
    title: "Last Volume",
    required: "false",
    id: 11
  },
  {
    title: "Total Volume",
    required: "false",
    id: 12
  },
  {
    title: "High",
    required: "false",
    id: 13
  },
  {
    title: "Low",
    required: "false",
    id: 14
  },
  {
    title: "52-week High/Low",
    required: "false",
    id: 15
  }
]

var AvailableProps = [
  {
    title: "Start Time",
    required: "false",
    id: 16
  },
  {
    title: "Stop Time",
    required: "false",
    id: 17
  },
  {
    title: "Per Point",
    required: "false",
    id: 18
  },
  {
    title: "Initial Margin",
    required: "false",
    id: 19
  }
]

var ItemList = React.createClass({
  render: function() {
    return (
      <div className="itemPicker">
        <div className="list-group col-md-6">
          <h2>Available Items</h2>
          <AvailableItems items = {AvailableProps} />
        </div>
        <div className="list-group col-md-6">
          <h2>Visible Items</h2>
          <VisibleItems items = {VisibleProps} />
        </div>
      </div>
    );
  }
});

export default ItemList
