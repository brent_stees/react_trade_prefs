import React from 'react'
import Item from './Item/index.jsx'

var VisibleItems = React.createClass({
  render: function() {
    return (
      <div className="visibleItems">
        <Item />
      </div>
    );
  }
});

export default VisibleItems
