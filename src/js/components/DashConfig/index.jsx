import React from 'react'
import ItemPicker from './ItemPicker/index.jsx'
import FormSubmit from './FormSubmit/index.jsx'

var DashConfig = React.createClass({
  render: function() {
    return (
      <div className="dashConfig">
        <div>
          <h1>Configure Data Fields</h1>
          <p>Drag & drop between columns to configure visible data.</p>
        </div>
        <ItemPicker />
        <FormSubmit />
      </div>
    );
  }
});

export default DashConfig
