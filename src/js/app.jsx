var React = require('react')
var DashConfig = require('./components/DashConfig/index.jsx')

window.onload = function() {
  React.render(
    <DashConfig />,
    document.getElementById('app')
  )
}